# Runner Notes

## Kubernetes Executor

The chart below describes the main runner pod properties: 
<https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml>

This main runner pod has one container that is responsible for creating the job pod in the cluster it resides. 
The job pod has at least 2 containers: helper and build. Helper is the container that handles everything that is not related to the job execution itself, for example, downloading the project's repository.
The build container is responsible for executing the job itself.

The are 2 important classes in the runner's code repository: 

- Executor
- ExecutorProvider

ExecutorProvider is the class that creates new Executor instances to execute new jobs
Executor is the class that executes the job itself

There's a defined Executor interface and each type of executor implements it. For example the kubernetes executor is an implementation of the Executor interface.
The Executor interface has the following main methods:
<https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/common/executor.go#L64-79>


```go
// Prepare prepares the environment for build execution. e.g. connects to SSH, creates containers.
Prepare(options ExecutorPrepareOptions) error
// Run executes a command on the prepared environment.
Run(cmd ExecutorCommand) error
// Finish marks the build execution as finished.
Finish(err error)
// Cleanup cleans any resources left by build execution.
Cleanup()
```

Here's the code for the job pod definition:
https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/executors/kubernetes/kubernetes.go#L1827-1866

This is where the main runner pod sends a request to the cluster to create the pod
https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/executors/kubernetes/kubernetes.go#L1662-1668

The runner logic only cares about jobs/builds. The pipeline is a GitLab instance concept


The multi.go class is the main wrapper around all runner tasks: getting the job from GitLab, and running it. Check the processBuildOnRunner() function
https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/commands/multi.go#L775-834

The build class controls the build execution itself. It delegates the execution to the executor. This class calls the methods defined in the Executor interface, like Run(), Finish() and Cleanup()
Check the Run() function
https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/common/build.go#L910-966


Ideas to troubleshoot slowness issues:

- In the main runner chart use a gitlab image that prints more information about the execution
- Get the messages they're getting and find the piece of code responsible for printing it, and troubleshoot from there
- Enable the runner debug mode