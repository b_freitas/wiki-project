# Introduction

This project was created as a placeholder for commands, scripts, studies, text files and etc.

## Existing documents

- [Create 3 GitLab instances in parallel](https://gitlab.com/b_freitas/wiki-project/-/wikis/Create-3-GitLab-instances-in-parallel)
- [Docker Gitaly Cluster](https://gitlab.com/b_freitas/wiki-project/-/wikis/Docker-Gitaly-Cluster)
- [Runner notes](https://gitlab.com/b_freitas/wiki-project/-/blob/main/Runner.md)